# Taskmasterd

### Commandes
- reload_config.go
    . voir la partie syscall.SIGHUP dans signal_handler.go
- restart_process.go
    . parcours la map taskasterd
    . selectionne le processus correspondant au nom 
    . kill le processus via le PID
    . relance le processus via la commande start_process
- get_process_status.go
    . parcours la map taskmasterd et affiche les noms + pid + status de tous les processus en cours 
- stop_main.go 
    . stop tous les processus enfant en cours 
    . stop le programme principal 
- stop_process.go 
    . parcours la map taskasterd et envoie un signal KILL avec le PID       correspondant au nom du process selectionné via la ligne de commande
    . si kill du process avec id 1 -> kill tous les process enfant (2,3,4...etc)

- faire une goroutine pour chaque process qui attend un signal définit dans le fichier de configuration afin d'arreter le programme proprement.

- récupérer les variables d'environnement du config file et les inclures dans les process

### Parsing 
- check_config_file.go
    . Vérifier que les données dans le fichier de conf sont correctes
        ex : pas de programs avec le même nom ...etc (à determiner)

### Logging

- Implémenter des fonctions afin d'améliorer le système de logging
    ex : max byte / fichier de log ...etc (à determiner) (Bonus)

---
