package main

import (
	"log"

	"gitlab.com/flmarsil/taskmaster/internal/domain/services/daemon"
)

const addr = ":44442"

func main() {
	// create new deamon service
	d := daemon.NewDaemonService()

	// parse file configuration
	configFile, err := d.RunParsing()
	if err != nil {
		log.Fatalf(err.Error())
	}

	// apply configuration from config file
	if err := d.RunConfig(configFile); err != nil {
		log.Fatalf(err.Error())
	}

	// open go routine for handling signals
	d.RunSignal()

	// run daemon server
	if err := d.RunServer(addr); err != nil {
		log.Fatalf(err.Error())
	}

}
