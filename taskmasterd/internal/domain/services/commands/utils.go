package commands

import (
	"container/list"
	"os"
	"syscall"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/logger"
)

/*
Check if a process already exist in taskmasterd programs map
*/
func GetProcessList(processName string, programs *map[string]*list.List) *list.List {
	for name, processList := range *programs {
		if processName == name {
			return processList
		}
	}
	return nil
}

func GetProcessNodeFromProcessList(name string, processList *list.List) *models.ProcessNode {
	for e := processList.Front(); e != nil; e = e.Next() {
		if e.Value.(*models.ProcessNode).Name == name {
			return e.Value.(*models.ProcessNode)
		}
	}
	return nil
}

func ExitProperly(programs *map[string]*list.List) error {
	for programName, list := range *programs {
		for e := list.Front(); e != nil; e = e.Next() {

			// get processus by PID
			proc, err := os.FindProcess(e.Value.(*models.ProcessNode).Pid)
			if err != nil {
				return err
			}

			// kill process properly
			proc.Signal(syscall.SIGTERM)

			logger.Taskmasterd.Printf("end process '%v' pid : '%v'\n", e.Value.(*models.ProcessNode).Name, e.Value.(*models.ProcessNode).Pid)
		}

		// delete program from map
		delete(*programs, programName)
	}

	return nil
}

func CreateWorkDir(path string, umask int) error {
	if path == "" {
		return nil
	}

	if _, err := os.Stat(path); os.IsNotExist(err) {
		old := syscall.Umask(umask)
		defer syscall.Umask(old)

		if err := os.MkdirAll(path, 0777); err != nil {
			return err
		}
	}
	return nil
}

func GetOutputLogs(dir, file string, isErr bool, umask int) (*os.File, error) {
	if file == "" && isErr {
		return os.Stderr, nil
	} else if file == "" && !isErr {
		return os.Stdout, nil
	}

	var openLogFile *os.File

	fullpath := dir + "/" + file

	if _, err := os.Stat(fullpath); os.IsNotExist(err) {
		old := syscall.Umask(umask)
		defer syscall.Umask(old)

		openLogFile, err = os.OpenFile(fullpath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			return nil, err
		}
	}

	return openLogFile, nil
}
