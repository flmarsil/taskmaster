package commands

import (
	"container/list"
	"fmt"
	"os"
	"strings"
	"syscall"
	"time"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/logger"
)

func StopProcess(cmd *models.CtlCommand, programs *map[string]*list.List) error {
	// stop main process
	if len(cmd.Args) > 1 && cmd.Args[1] == "taskmasterd" {

		mainPid := os.Getpid()

		proc, err := os.FindProcess(mainPid)
		if err != nil {
			fmt.Println(err)
		}

		proc.Signal(syscall.SIGTERM)

		cmd.Response = []byte("taskmasterd has been stopped")
		return nil

	} else if cmd.Program == nil {
		return fmt.Errorf("no program to stop has been specified")
	}

	var processList *list.List

	splitProcessName := strings.Split(cmd.Args[1], "-")

	if processList = GetProcessList(splitProcessName[0], programs); processList == nil {
		return fmt.Errorf("no processes are running")
	}
	// stop one process only with id (ex: flmarsil-1)
	if len(splitProcessName) > 1 {
		if processNode := GetProcessNodeFromProcessList(cmd.Args[1], processList); processNode == nil {
			return fmt.Errorf("process does not exist")
		} else {
			return checkProcessStatusBeforeStop(cmd, processNode, programs)
		}
		// stop all process node running or process with no id (ex : flmarsil)
	} else if len(splitProcessName) == 1 || cmd.Program.NumProcs > 1 {
		for e := processList.Front(); e != nil; e = e.Next() {
			e.Value.(*models.ProcessNode).Status = models.Stopping
			go func(e *list.Element) {
				StopProcessFromProcessNode(cmd, e.Value.(*models.ProcessNode), programs, false)
			}(e)
		}

		// delete processList from program map
		delete(*programs, splitProcessName[0])
	}

	cmd.Response = []byte("program has been stopped")
	return nil
}

func deleteProcessFromMap(name string, programs *map[string]*list.List) error {
	// split dash number in case of multiple process numbers
	splitProcessName := strings.Split(name, "-")

	for programName, list := range *programs {
		// check program name which match with split process name
		if programName == splitProcessName[0] {
			for e := list.Front(); e != nil; e = e.Next() {
				if e.Value.(*models.ProcessNode).Name == name {
					list.Remove(e)
				}
			}
		}
	}
	return nil
}

func StopProcessFromProcessNode(cmd *models.CtlCommand, processNode *models.ProcessNode, programs *map[string]*list.List, forceRestart bool) error {
	// wait x sec before stop
	if cmd.Program.StopWaitSecs != 0 {
		time.Sleep(time.Second * time.Duration(cmd.Program.StopWaitSecs))
	}

	// get processus by PID
	proc, err := os.FindProcess(processNode.Pid)
	if err != nil {
		return err
	}

	// kill process properly
	proc.Signal(syscall.SIGTERM)

	logger.Taskmasterd.Printf("end process '%v' pid : '%v'\n", processNode.Name, processNode.Pid)

	// check auto restart
	if cmd.Program.AutoRestart == "always" || forceRestart {
		cmd.Id = models.CMD_START
		processNode.Pid = 0
		processNode.Status = models.Restarting
		cmd.Response = []byte("")
		return StartProcess(cmd, programs)
	}

	deleteProcessFromMap(processNode.Name, programs)

	return nil
}

func checkProcessStatusBeforeStop(cmd *models.CtlCommand, processNode *models.ProcessNode, programs *map[string]*list.List) error {
	switch processNode.Status {
	case models.Running:
		processNode.Status = models.Stopping
		go StopProcessFromProcessNode(cmd, processNode, programs, false)
		cmd.Response = []byte("program has been stopped")
		return nil
	case models.Waiting:
		processNode.Status = models.Stopping
		deleteProcessFromMap(processNode.Name, programs)
		cmd.Response = []byte("program has been stopped")
		return nil
	case models.Processing:
		return fmt.Errorf("program is in processing, retry later")
	case models.Stopping:
		return fmt.Errorf("program is already stopping")
	default:
		return nil
	}
}
