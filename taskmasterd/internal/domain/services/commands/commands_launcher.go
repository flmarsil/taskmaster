package commands

import (
	"container/list"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/logger"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/parser"
)

func CommandLauncher(cmd *models.CtlCommand, programs *map[string]*list.List, ctl *models.CtlConn, config *models.ConfigFile, flags *models.Flags) error {
	cmd.Response = nil

	switch cmd.Id {
	case models.CMD_STATUS:
		return GetProcessStatus(cmd, programs, ctl)
	case models.CMD_RELOAD:
		// reparse configuration file from original path to get new config
		newConfig, err := parser.ParseConfigFile(flags.Cflag)
		if err != nil {
			return err
		}
		logger.Taskmasterd.Printf("reloading the configuration file")
		return ReloadConfig(cmd, programs, ctl, config, newConfig)
	case models.CMD_RESTART:
		return RestartProcess(cmd, programs)
	case models.CMD_START:
		return StartProcess(cmd, programs)
	case models.CMD_STOP:
		return StopProcess(cmd, programs)
	}
	return nil
}
