package commands

import (
	"container/list"
	"fmt"
	"os"
	"strings"
	"syscall"
	"time"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/logger"
)

/*
Program Start Manager
- Checks if the program to be launched is already running
- Launches programs already on the waiting list
- Puts programs on waiting lists
- Starts programs directly
*/
func StartProcess(cmd *models.CtlCommand, programs *map[string]*list.List) error {
	if cmd.Program == nil {
		return fmt.Errorf("no program specified or no program exists")
	}

	var processList *list.List

	// check if the program we want to start already exist
	if processList = GetProcessList(cmd.Program.ProcessName, programs); processList != nil {
		if selectedProcessNode := GetProcessNodeFromProcessList(cmd.Args[1], processList); selectedProcessNode == nil {
			return fmt.Errorf("process does not exist")
		} else {
			return checkProcessStatusBeforeStart(cmd, selectedProcessNode)
		}
	} else {
		processList = list.New()
	}

	putProcessOnWaitingList(processList, cmd, programs)

	// program must be started manually
	if !cmd.FromShell && !cmd.Program.AutoStart {
		return nil
	}

	for elem := processList.Front(); elem != nil; elem = elem.Next() {
		go func(elem *list.Element) {
			RunProcessFromProcessNode(cmd, elem.Value.(*models.ProcessNode))
		}(elem)
	}

	cmd.Response = []byte("program has been started")
	return nil
}

func checkProcessStatusBeforeStart(cmd *models.CtlCommand, processNode *models.ProcessNode) error {
	switch processNode.Status {
	case models.Running:
		return fmt.Errorf("program is already running")
	case models.Waiting, models.Restarting:
		processNode.Status = models.Processing
		go RunProcessFromProcessNode(cmd, processNode)
		cmd.Response = []byte("program has been started")
		return nil
	case models.Processing:
		return fmt.Errorf("launch of the program is in progress")

	default:
		return nil
	}
}

/*
Creating the waiting lists with each program from config file.
*/
func putProcessOnWaitingList(processList *list.List, cmd *models.CtlCommand, programs *map[string]*list.List) {
	var processName string

	commandName := strings.Split(cmd.Program.Command, " ")

	for i := 1; i <= int(cmd.Program.NumProcs); i += 1 {
		if cmd.Program.NumProcs > 1 {
			processName = cmd.Program.ProcessName + "-" + fmt.Sprint(i)
		} else {
			processName = cmd.Program.ProcessName
		}

		processNode := &models.ProcessNode{
			Pid:     0,
			Name:    processName,
			Status:  models.Waiting,
			Command: commandName[0],
		}

		processList.PushBack(processNode)
	}
	(*programs)[cmd.Program.ProcessName] = processList

}

/*
Takes a process node and run it
*/
func RunProcessFromProcessNode(cmd *models.CtlCommand, processNode *models.ProcessNode) {
	childPid, status, err := processLauncher(cmd)
	if err != nil {
		// log ?
	} else {

		logger.Taskmasterd.Printf("create process : '%v' pid : '%v'\n", processNode.Name, childPid.Pid)

		chWs := make(chan syscall.WaitStatus, 1)
		chPs := make(chan *os.ProcessState, 1)

		go func(childPid *os.Process) {
			var ps *os.ProcessState

			ps, err = childPid.Wait()
			if err != nil {
				fmt.Println(err)
			} else {
				// fmt.Println("******")
				// fmt.Println("pid         : ", ps.Pid())
				// fmt.Println("exited      : ", ps.Exited())
				// fmt.Println("exit code   : ", ps.ExitCode())
				// fmt.Println("string      : ", ps.String())
				// fmt.Println("success     : ", ps.Success())
				// fmt.Println("wait status : ", ps.Sys().(syscall.WaitStatus))
				// fmt.Println("******")

				chPs <- ps
				chWs <- ps.Sys().(syscall.WaitStatus)
			}
		}(childPid)
		processNode.Pid = childPid.Pid
		processNode.Status = status
		processNode.WaitStatus = <-chWs
		processNode.Ps = <-chPs
	}
}

/*
Launch each process taking into account the constraints indicated in the configuration file
*/
func processLauncher(cmd *models.CtlCommand) (*os.Process, string, error) {
	// wait x sec before start
	if cmd.Program.StartSecs != 0 {
		time.Sleep(time.Second * time.Duration(cmd.Program.StartSecs))
	}

	// create program working dir
	if err := CreateWorkDir(cmd.Program.Directory, int(cmd.Program.Umask)); err != nil {
		return nil, models.Error, err
	}

	// get file descriptor for stdout process
	stdOutLogFileFd, err := GetOutputLogs(cmd.Program.Directory, cmd.Program.StdOutLogFile, false, int(cmd.Program.Umask))
	if err != nil {
		return nil, models.Error, err
	}

	// get file descriptor for stderr process
	stdErrLogFileFd, err := GetOutputLogs(cmd.Program.Directory, cmd.Program.StdErrLogFile, true, int(cmd.Program.Umask))
	if err != nil {
		return nil, models.Error, err
	}

	// spliting command by spaces (arguments)
	cmd.Args = strings.Split(cmd.Program.Command, " ")

	// saving retries number from config file
	startRetries := cmd.Program.StartRetries

RETRY:
	// forking process and exec command from config file
	childPid, err := os.StartProcess(cmd.Args[0], cmd.Args, &os.ProcAttr{
		Dir: cmd.Program.Directory,
		Env: append(os.Environ(), cmd.Program.Environnement...),
		Sys: &syscall.SysProcAttr{
			Setsid: true,
		},
		Files: []*os.File{os.Stdin, stdOutLogFileFd, stdErrLogFileFd},
	})

	// retrying to launch program in case of failure
	if err != nil {
		startRetries -= 1
		if startRetries > 0 {
			logger.Taskmasterd.Printf("retrying : process '%v' failed to start : %v\n", cmd.Program.ProcessName, err)
			goto RETRY
		}
		return nil, models.Error, err
	}

	return childPid, models.Running, nil
}
