package commands

import (
	"fmt"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
)

func CheckCommandValidity(cmd *models.CtlCommand) error {
	var err error

	if len(cmd.Args) > 2 {
		return fmt.Errorf("too many arguments")
	}

	command := models.Command(cmd.Args[0])

	cmd.Id, err = models.Command.CheckCommand(command)
	if err != nil {
		cmd.Response = []byte(err.Error())
		return err
	}

	return nil
}
