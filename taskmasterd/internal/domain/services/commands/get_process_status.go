package commands

import (
	"container/list"
	"fmt"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
)

func GetProcessStatus(cmd *models.CtlCommand, programs *map[string]*list.List, ctl *models.CtlConn) error {
	// var out []string
	out := make([]string, 0)
	out = append(out, fmt.Sprintf("\n%v \t %v \t\t %v\n\n", "PID", "NAME", "STATUS"))

	for _, list := range *programs {
		for e := list.Front(); e != nil; e = e.Next() {
			out = append(out, fmt.Sprintf("%v\t %v \t %v\n", e.Value.(*models.ProcessNode).Pid, e.Value.(*models.ProcessNode).Name, e.Value.(*models.ProcessNode).Status))
		}
	}

	res := ""
	for _, s := range out {
		res = res + fmt.Sprint(s)
	}

	cmd.Response = []byte(res)

	return nil
}
