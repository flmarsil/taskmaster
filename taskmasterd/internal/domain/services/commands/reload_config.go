package commands

import (
	"bytes"
	"container/list"
	"encoding/gob"
	"log"
	"os"
	"strings"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/logger"
)

/*
Configuration reload manager
- Compares the new configuration with the current one part by part
- Applies detected changes
*/
func ReloadConfig(cmd *models.CtlCommand, programs *map[string]*list.List, ctl *models.CtlConn, curr, new *models.ConfigFile) error {
	if isTaskmasterd := checkConfigDiff(curr.Taskmasterd, new.Taskmasterd); isTaskmasterd {
		if err := applyTaskmasterdChanges(curr, new); err != nil {
			return err
		}
	}

	if isTaskmasterctl := checkConfigDiff(curr.Taskmasterctl, new.Taskmasterctl); isTaskmasterctl {
		if err := applyTaskmasterctlChanges(ctl, curr, new); err != nil {
			return err
		}
	}

	if isPrograms := checkConfigDiff(curr.Programs, new.Programs); isPrograms {
		toDoProcess, err := prepareWhatToDoWithProcesses(&curr.Programs, &new.Programs, programs)
		if err != nil {
			return err
		}

		if err := applyProgramsChanges(toDoProcess, programs, ctl, curr, new); err != nil {
			return err
		}
	}

	if isPrograms := checkConfigDiff(curr.Programs, new.Programs); isPrograms {
		toDoProcess, err := prepareWhatToDoWithProcesses(&curr.Programs, &new.Programs, programs)
		if err != nil {
			return err
		}

		if err := applyProgramsChanges(toDoProcess, programs, ctl, curr, new); err != nil {
			return err
		}
	}

	if cmd != nil {
		// if len(cmd.Args) > 1 {
		// if cmd.Args[1] == "-f" {
		// 	newCmd := &models.CtlCommand{
		// 		Id:        cmd.Id,
		// 		Args:      cmd.Args[:1],
		// 		Program:   cmd.Program,
		// 		Response:  cmd.Response,
		// 		FromShell: cmd.FromShell,
		// 	}
		// ReloadConfig(newCmd, programs, ctl, &models.ConfigFile{}, new)
		// goto NORESP

		// }
		// else {
		// return fmt.Errorf("too many arguments")
		// }
		cmd.Response = []byte("configuration has been reloaded")
	}

	return nil
}

func applyTaskmasterdChanges(curr, new *models.ConfigFile) error {
	var err error

	// set environnement varibles from config file
	for _, v := range new.Taskmasterd.Environnement {
		env := strings.Split(v, "=")

		name := env[0]
		value := strings.Replace(env[1], `"`, "", -1)

		os.Setenv(name, value)
	}

	logger.Taskmasterd, err = logger.CreateTaskLogFile(new.Taskmasterd.Directory, new.Taskmasterd.LogFile, int(new.Taskmasterd.Umask))
	if err != nil {
		return err
	}

	curr.Taskmasterd = new.Taskmasterd
	return nil
}

/*
In the case of modifications on the taskmasterctl :
- closes the current session in order to ask the user for the new identifiers in the configuration file.
- saves the new configuration
*/
func applyTaskmasterctlChanges(ctl *models.CtlConn, curr, new *models.ConfigFile) error {
	if ctl.Conn != nil {
		ctl.IsConn = false
		ctl.Conn.Close()
	}
	// save new configuration
	curr.Taskmasterctl = new.Taskmasterctl
	return nil
}

/*
After detecting the changes, this function applies them according to the toDoProcess map
*/
func applyProgramsChanges(toDoProcess *map[string]models.CmdID, programs *map[string]*list.List, ctl *models.CtlConn, currConfig, newConfig *models.ConfigFile) error {
	// var wg sync.WaitGroup
	for processName, toDo := range *toDoProcess {

		command := &models.CtlCommand{}

		// Retrieves the configuration of the program concerned
		if toDo == models.CMD_STOP {
			for _, currProgram := range currConfig.Programs {
				if currProgram.ProcessName == processName {
					command.Program = &currProgram
				}
			}
		} else {
			for _, newProgram := range newConfig.Programs {
				if newProgram.ProcessName == processName {
					// ! creating new variable on stack to get a new pointer address
					newStackPointerProgram := newProgram
					command.Program = &newStackPointerProgram
				}
			}
		}

		command.Id = toDo
		command.Args = make([]string, 2)
		command.Args[1] = processName

		// wg.Add(1)
		// go func(command *models.CtlCommand) {
		// defer wg.Done()

		err := CommandLauncher(command, programs, ctl, nil, nil)
		if err != nil {
			// log.
		}

		// }(command)
	}

	// wg.Wait()

	// save new programs configuration
	currConfig.Programs = newConfig.Programs
	return nil
}

/*
Creates map that contain name of programs and corresponding command id for knowing what to do with processes.

Restart, add programs
- Go through all the programs in the new configuration file.
- For each new program, check if it is present in the current configuration file.
- If there is a difference, the program must be restarted with new configuration.
- If there is no difference, do nothing.
- If a new program is not present in the current configuration, it must be added.

Deleting programs
- Go through all the programs in the current configuration file.
- If a program is not present in the toDoProcess list, it must be stopped.
*/
func prepareWhatToDoWithProcesses(currPrograms, newPrograms *[]models.Program, programs *map[string]*list.List) (*map[string]models.CmdID, error) {
	toDoProcess := make(map[string]models.CmdID)

	var isFound bool
	// browses the new configuration and look for current existing programs in current configuration to detect changes.
	for _, newProgram := range *newPrograms {
		isFound = false
		for _, currProgram := range *currPrograms {
			if newProgram.ProcessName == currProgram.ProcessName {
				// compare current and new program with the same name
				if isDiff := checkConfigDiff(newProgram, currProgram); isDiff {
					if processList := GetProcessList(newProgram.ProcessName, programs); processList == nil {
						toDoProcess[newProgram.ProcessName] = models.CMD_START
					} else {
						// some differences are detected, restarting with new config is necessary
						toDoProcess[newProgram.ProcessName] = models.CMD_RESTART
					}
				} else {
					// do nothing, there are no changes between current and new program
					toDoProcess[newProgram.ProcessName] = models.CMD_NOTHING
				}
				isFound = true
				break
			}
		}
		// if the program in the new configuration file is not found in the current configuration,
		// it means that it must be added
		if !isFound {
			toDoProcess[newProgram.ProcessName] = models.CMD_START
		}
	}

	// searchs in the current configuration all the programs that do not appear in the toDoProcess map :
	// this means that they must be deleted
	var toDel bool
	for _, currProgram := range *currPrograms {
		toDel = true
		for processName := range toDoProcess {
			// process name match, so it must not be deleted
			if currProgram.ProcessName == processName {
				toDel = false
				break
			}
		}

		// the program is not in the new configuration it means that it must be deleted
		if toDel {
			toDoProcess[currProgram.ProcessName] = models.CMD_STOP
		}
	}

	return &toDoProcess, nil
}

/*
Compare two parts of a configuration file
- Encode two interfaces in bytes array
- Compare two bytes array interfaces for detect differences
*/
func checkConfigDiff(curr, new interface{}) bool {
	isDiff := bytes.Compare(encodeToBytes(new),
		encodeToBytes(curr))
	if isDiff != 0 {
		return true
	} else {
		return false
	}
}

/*
- Encode interface in bytes array
*/
func encodeToBytes(p interface{}) []byte {
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(p)
	if err != nil {
		// TODO: exit in case of error or not ?
		log.Fatal(err)
	}
	return buf.Bytes()
}
