package commands

import (
	"container/list"
	"fmt"
	"strings"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/logger"
)

func RestartProcess(cmd *models.CtlCommand, programs *map[string]*list.List) error {
	if len(cmd.Args) != 2 {
		return fmt.Errorf("no process to restart has been specified")
	}

	var processList *list.List
	var processNode *models.ProcessNode

	splitProcessName := strings.Split(cmd.Args[1], "-")

	if processList = GetProcessList(splitProcessName[0], programs); processList == nil {
		return fmt.Errorf("no process corresponding for this name")
	}

	logger.Taskmasterd.Println("process restart request received")

	// restart one process only
	if len(splitProcessName) > 1 {
		if processNode = GetProcessNodeFromProcessList(cmd.Args[1], processList); processNode == nil {
			return fmt.Errorf("no process corresponding for this name")
		}

		processNode.Status = models.Stopping

		go StopProcessFromProcessNode(cmd, processNode, programs, true)

		cmd.Response = []byte("program has been restarted")
		return nil
	}

	// restart all process
	// stop all process node running
	for e := processList.Front(); e != nil; e = e.Next() {
		e.Value.(*models.ProcessNode).Status = models.Stopping
		go func(e *list.Element) {
			StopProcessFromProcessNode(cmd, e.Value.(*models.ProcessNode), programs, false)
		}(e)
	}

	// delete processList from program map
	delete(*programs, splitProcessName[0])

	cmd.Id = models.CMD_START

	return StartProcess(cmd, programs)
}
