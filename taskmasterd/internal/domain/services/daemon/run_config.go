package daemon

import (
	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/commands"
)

/*
Apply the configuration passed in argument
*/
func (d *daemon) RunConfig(config *models.ConfigFile) error {
	if err := commands.ReloadConfig(nil, &d.programs, d.ctl, d.config, config); err != nil {
		return err
	}
	return nil
}
