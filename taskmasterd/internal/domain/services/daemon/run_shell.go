package daemon

import (
	"crypto/sha1"
	"fmt"
	"io"
	"strings"
)

/*
Checks the credentials received from the client to authorize or not the connection.
- Checks if the password in the configuration file is hashed.
- Receives input from the client.
- Compares the received credentials with those in the configuration file.
*/
func (d *daemon) RunShell() error {
	// sending custom shell prompt to the client from config file
	d.ctl.Conn.Write([]byte(d.config.Taskmasterctl.Prompt))

	isPwdEncrypted := false
	sha1Pwd := ""

	if strings.Contains(d.config.Taskmasterctl.Password, "{SHA}") {
		isPwdEncrypted = true
		sha1Pwd = strings.TrimPrefix(d.config.Taskmasterctl.Password, "{SHA}")
	}

	buf := make([]byte, 512)

	for !d.ctl.IsConn {

		nBytes, err := d.ctl.Conn.Read(buf)
		if err != nil {
			if err != io.EOF {
				closeConnection(d.ctl, d.config)
				return fmt.Errorf("RunShell  error : %v", err)
			}
		}

		// credential[0] = username
		// credential[1] = password
		credentials := strings.Split(string(buf[0:nBytes]), ":")

		// Verify that the identifiers match those in the configuration file
		if ((!isPwdEncrypted && credentials[1] == d.config.Taskmasterctl.Password) || (isPwdEncrypted && sha1Pwd == fmt.Sprintf("%x", sha1.Sum([]byte(credentials[1]))))) && credentials[0] == d.config.Taskmasterctl.Username {
			d.ctl.IsConn = true
			d.ctl.Conn.Write([]byte("ok"))
			break
		}

		d.ctl.Conn.Write([]byte("nok"))
	}

	return nil
}
