package daemon

import (
	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/parser"
)

/*
Parser from Taskasmterd program
- Get the flags from the command line or default path
- Starts parsing the configuration file
*/
func (d *daemon) RunParsing() (*models.ConfigFile, error) {
	err := parser.ParseFlags(d.flags)
	if err != nil {
		return nil, err
	}

	config, err := parser.ParseConfigFile(d.flags.Cflag)
	if err != nil {
		return nil, err
	}

	return config, nil
}
