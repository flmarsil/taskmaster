package daemon

import (
	"crypto/rand"
	"crypto/tls"
	"fmt"
	"io"
	"log"
	"net"
	"strings"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
)

/*
Server of the Taskmasterd program.
- Initiates a tls connection on a given port.
- Accepts incoming connections.
- Opens an authentication system to send a command shell back to the client.
- Receives commands from the client.
- Run the received commands.
*/
func (d *daemon) RunServer(addr string) error {
	var err error

	d.ctl.Listener, err = initListener(addr)
	if err != nil {
		log.Fatalf(err.Error())
	}
	defer d.ctl.Listener.Close()

	buf := make([]byte, 4096)

	for {

		d.ctl.Conn, err = d.ctl.Listener.Accept()
		if err != nil {
			log.Fatalf(err.Error())
		}

		// verify shell connection validity from taskmasterctl
		if !d.ctl.IsConn && d.config.Taskmasterctl.Password != "" {
			if err := d.RunShell(); err != nil {
				closeConnection(d.ctl, d.config)
				log.Fatalf(err.Error())
			}
			fmt.Printf("new connection from %v\n", d.ctl.Conn.RemoteAddr())
		}

		for {
			nBytes, err := d.ctl.Conn.Read(buf)
			if err != nil {
				closeConnection(d.ctl, d.config)
				if err != io.EOF {
					log.Printf("connection read error : %v\n", err)

				}

				break
			}

			command := &models.CtlCommand{}

			command.Args = strings.Split(string(buf[0:nBytes]), " ")

			if err := d.RunCommand(command); err != nil {
				d.ctl.Conn.Write([]byte(err.Error()))
			} else {
				d.ctl.Conn.Write(command.Response)
			}

		}
	}
}

// init secure connection configuration for daemon server
func initListener(addr string) (net.Listener, error) {
	cert, err := tls.LoadX509KeyPair("certs/taskmasterd.pem", "certs/taskmasterd.key")
	if err != nil {
		log.Fatalf(err.Error())
	}

	config := tls.Config{Certificates: []tls.Certificate{cert}}
	config.Rand = rand.Reader

	listener, err := tls.Listen("tcp", addr, &config)
	if err != nil {
		log.Fatalf(err.Error())
	}

	fmt.Printf("waiting for a connection on %v\n", listener.Addr())
	return listener, nil
}

func closeConnection(ctl *models.CtlConn, config *models.ConfigFile) {
	// if password auth is configured, disconnect current user
	if config.Taskmasterctl.Password != "" {
		ctl.IsConn = false
	}

	ctl.Conn.Close()
}
