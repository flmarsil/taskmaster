package daemon

import (
	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/signal"
)

func (d *daemon) RunSignal() {
	// preparing the order ID for when we will receive a SIGHUP
	cmd := &models.CtlCommand{Id: models.CMD_RELOAD}

	go signal.SignalHandler(cmd, &d.programs, d.ctl, d.config, d.flags)
	go signal.ChildPidChecker(cmd, &d.programs, d.ctl, d.config, d.flags)
}
