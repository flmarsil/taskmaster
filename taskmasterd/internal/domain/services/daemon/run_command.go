package daemon

import (
	"strings"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/commands"
)

/*
Taskmasterd command manager.
- Checks the validity of the command received by the client.
- Launches the function corresponding to the received command.
*/
func (d *daemon) RunCommand(cmd *models.CtlCommand) error {
	if err := commands.CheckCommandValidity(cmd); err != nil {
		return err
	}

	// when we use command shell from taskmasterctl, programs must be
	// parsed for getting configuration.
	if len(cmd.Args) > 1 && cmd.Program == nil {
		// split dash number in case of multiple process numbers
		splitProcessName := strings.Split(cmd.Args[1], "-")
		for _, program := range d.config.Programs {
			if program.ProcessName == splitProcessName[0] {
				newStackPointerProgram := program
				cmd.Program = &newStackPointerProgram
				cmd.FromShell = true
			}
		}

	}

	if err := commands.CommandLauncher(cmd, &d.programs, d.ctl, d.config, d.flags); err != nil {
		return err
	}

	return nil
}
