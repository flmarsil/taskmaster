package daemon

import (
	"container/list"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
)

/*
Main service of the Taskmasterd program.
- Starts a server waiting for a client connection.
- Starts a shell to allow the client to authenticate.
- Executes the commands received by the client.
*/
type Daemon interface {
	RunServer(addr string) error
	RunShell() error
	RunCommand(cmd *models.CtlCommand) error
	RunConfig(config *models.ConfigFile) error
	RunParsing() (*models.ConfigFile, error)
	RunSignal()
}

type daemon struct {
	// current configuration file applyied
	config *models.ConfigFile
	// keeps track of all programs managed by the daemon
	programs map[string]*list.List
	// keeps track of taskmasterctl connection
	ctl *models.CtlConn
	// keeps track of flags
	flags *models.Flags
}

func NewDaemonService() Daemon {
	return &daemon{config: &models.ConfigFile{}, ctl: &models.CtlConn{Conn: nil, IsConn: false}, programs: make(map[string]*list.List), flags: &models.Flags{}}
}
