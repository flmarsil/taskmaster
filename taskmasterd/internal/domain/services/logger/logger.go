package logger

import (
	"log"
	"os"
	"syscall"
)

// var TaskLogger *log.Logger

var Taskmasterd = &log.Logger{}

func CreateTaskLogFile(dir, file string, umask int) (*log.Logger, error) {
	var openLogFile *os.File

	if dir != "" {
		if _, err := os.Stat(dir); os.IsNotExist(err) {
			old := syscall.Umask(umask)
			defer syscall.Umask(old)

			if err := os.MkdirAll(dir, 0777); err != nil {
				return nil, err
			}
		}
	}

	if file != "" {

		fullpath := dir + "/" + file

		if _, err := os.Stat(fullpath); os.IsNotExist(err) {
			old := syscall.Umask(umask)
			defer syscall.Umask(old)

			openLogFile, err = os.OpenFile(fullpath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
			if err != nil {
				return nil, err
			}
		}

	}

	return log.New(openLogFile, "taskmasterd:", log.Ldate|log.Ltime), nil
}
