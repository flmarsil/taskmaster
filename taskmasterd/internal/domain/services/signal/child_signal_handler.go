package signal

import (
	"container/list"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/commands"
)

/*
Monitor child process wait status
*/

func ChildSignalHandler(cmd *models.CtlCommand, programs *map[string]*list.List, ctl *models.CtlConn, config *models.ConfigFile, flags *models.Flags) {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)

	for {
		select {
		case s := <-signalChan:
			switch s {
			case syscall.SIGINT, syscall.SIGTERM:
				// TODO: log
				fmt.Println("got SIGINT/SIGTERM, exiting ...")
				// TOOD: exit properly, close all process
				close(signalChan)
				os.Exit(int(cmd.Program.ExitCodes))
			case syscall.SIGHUP:
				fmt.Println("got SIGHUP, reloading ...")
				// TODO: log
				// restart process
				cmd.Id = models.CMD_RESTART
				commands.CommandLauncher(cmd, programs, ctl, config, flags)
			}
		}
	}
}
