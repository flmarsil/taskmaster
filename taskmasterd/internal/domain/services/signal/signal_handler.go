package signal

import (
	"container/list"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/commands"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/logger"
)

func SignalHandler(cmd *models.CtlCommand, programs *map[string]*list.List, ctl *models.CtlConn, config *models.ConfigFile, flags *models.Flags) {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)

	for {
		select {
		case s := <-signalChan:
			switch s {
			case syscall.SIGINT, syscall.SIGTERM:

				logger.Taskmasterd.Println("(TERM/INT) taskmasterd has been stopped")

				// send signal term to all running processes and clean up taskmasterd map
				commands.ExitProperly(programs)

				if ctl.Conn != nil {
					ctl.Conn.Close()
					ctl.IsConn = false
				}

				close(signalChan)
				os.Exit(1)
			case syscall.SIGHUP:
				// TODO: log
				fmt.Println("got SIGHUP, reloading ...")
				// reloading configuration
				commands.CommandLauncher(cmd, programs, ctl, config, flags)
			}
		}
	}
}
