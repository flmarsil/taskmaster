package signal

import (
	"container/list"
	"fmt"
	"os"
	"strings"
	"syscall"
	"time"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/commands"
	"gitlab.com/flmarsil/taskmaster/internal/domain/services/logger"
)

/*
Checks the status of child PIDs every x seconds.
If the PID no longer exists, it is removed from the programs map.
*/

func ChildPidChecker(cmd *models.CtlCommand, programs *map[string]*list.List, ctl *models.CtlConn, config *models.ConfigFile, flags *models.Flags) {

	for {
		// wait 3 sec
		time.Sleep(time.Second * 3)

		for progName, list := range *programs {

			// all nodes from list process has been removed, so we delete program from map
			if list.Front() == nil {
				delete(*programs, progName)
				break
			}

			for elem := list.Front(); elem != nil; elem = elem.Next() {
				if elem.Value.(*models.ProcessNode).Status == models.Running {

					progName := strings.Split(elem.Value.(*models.ProcessNode).Name, "-")

					// get program data
					var program *models.Program
					for _, prog := range config.Programs {
						if prog.ProcessName == progName[0] {
							program = &prog
							break
						}
					}

					// verify if wait signal received correspond to properly stop signal from config program
					if GetSignalValue(program.StopSignal) == int(elem.Value.(*models.ProcessNode).WaitStatus) {
						go func(elem *models.ProcessNode) {
							elem.Status = models.Stopping

							if program.StopWaitSecs != 0 {
								time.Sleep(time.Second * time.Duration(program.StopWaitSecs))
							}

							proc, err := os.FindProcess(elem.Ps.Pid())
							if err != nil {
								fmt.Println(err)
							}

							proc.Signal(syscall.SIGTERM)

							logger.Taskmasterd.Printf("program '%v' with pid '%v' has been received '%v' signal and has been stopped : exit code : '%v' \n", elem.Name, elem.Pid, program.StopSignal, program.ExitCodes)

							elem.WaitStatus = 0

						}(elem.Value.(*models.ProcessNode))
						list.Remove(elem)

					} else {
						// send null signal to child process. If err, it does not exist yet
						err := syscall.Kill(elem.Value.(*models.ProcessNode).Pid, 0)
						if err != nil {
							elem.Value.(*models.ProcessNode).Status = models.Terminated
							// program is terminated, restart it if auto restart policy is always
							if program.AutoRestart == "always" {
								cmd.Id = models.CMD_START
								cmd.Program = program
								cmd.Response = []byte("")

								elem.Value.(*models.ProcessNode).Status = models.Restarting

								go func(processNode *models.ProcessNode) {
									commands.RunProcessFromProcessNode(cmd, processNode)
								}(elem.Value.(*models.ProcessNode))

							} else {
								logger.Taskmasterd.Printf("program '%v' with pid '%v' ended properly\n", elem.Value.(*models.ProcessNode).Name, elem.Value.(*models.ProcessNode).Pid)
								list.Remove(elem)
							}
						}
					}

				}
			}
		}
	}
}

const (
	SIGTERM = 15
	SIGHUP  = 1
	SIGINT  = 2
	SIGQUIT = 3
	SIGKILL = 9
)

func GetSignalValue(signal string) int {
	switch signal {
	case "TERM":
		return SIGTERM
	case "HUP":
		return SIGHUP
	case "INT":
		return SIGINT
	case "QUIT":
		return SIGQUIT
	case "KILL":
		return SIGKILL
	default:
		return 0
	}
}
