package parser

import (
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
	yaml "gopkg.in/yaml.v2"
)

/*
Parse the configuration file
- Opens the configuration file
- Retrieves its content
- Fills the corresponding structures with the recovered data.
- Checks the validity of the recovered data.
*/
func ParseConfigFile(configFilePath *string) (*models.ConfigFile, error) {
	file, err := os.Open(*configFilePath)
	if err != nil {
		return nil, fmt.Errorf("error when trying to open configuration file: %v", err)
	}
	defer file.Close()

	content, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, fmt.Errorf("error when trying to read content from configuration file: %v", err)
	}

	configFile := &models.ConfigFile{
		Path: *configFilePath,
	}

	err = yaml.Unmarshal(content, &configFile)
	if err != nil {
		return nil, fmt.Errorf("error when trying to unmarshal content from configuration file: %v", err)
	}

	err = CheckConfigFile(configFile)
	if err != nil {
		return nil, err
	}

	return configFile, nil
}

// TODO: check config file validity
func CheckConfigFile(configFile *models.ConfigFile) error {
	if configFile.Programs == nil {
		return fmt.Errorf("there are no programs in config file")
	}
	return nil
}
