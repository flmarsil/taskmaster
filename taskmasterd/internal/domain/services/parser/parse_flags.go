package parser

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/flmarsil/taskmaster/internal/domain/models"
)

/*
Parser of flag
- Get the default path of the configuration file
- Parse the flags from the command line
*/
func ParseFlags(flags *models.Flags) error {
	// TODO: get process owner : root ?

	// The flag.String function below does not allow the flag to be redefined once
	// it has been registered during a SIGHUP signal. In order to avoid a panic,
	// we do not want the function to be rerun if the Flag structure already exists.
	if flags.Cflag != nil {
		fmt.Println("PROTECT AGAINST REDEFINING C FlAG")
		return nil
	}

	defaultConfigPath, err := GetDefaultConfigFilePath()
	if err != nil {
		return fmt.Errorf("error when trying to get the default conf file path: %v", err)
	}

	// check flags
	flags.Cflag = flag.String("c", *defaultConfigPath, "absolute path to taskmasterd config file")
	// check others flags here ...
	flag.Parse()

	return nil
}

func GetDefaultConfigFilePath() (*string, error) {
	currentDir, err := os.Getwd()
	if err != nil {
		return nil, fmt.Errorf("error when trying to get the current directory: %v", err)
	}

	defaultDir := currentDir + "/config/taskmasterd.conf"

	return &defaultDir, nil
}
