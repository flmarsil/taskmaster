package models

import (
	"fmt"
	"net"
)

type CmdID int

type Command string

const (
	CmdStatus  Command = "status"
	CmdReload  Command = "reload"
	CmdRestart Command = "restart"
	CmdStart   Command = "start"
	CmdStop    Command = "stop"
)

const (
	CMD_STATUS CmdID = iota
	CMD_RELOAD
	CMD_RESTART
	CMD_START
	CMD_STOP
	CMD_UNKNOWN
	CMD_NOTHING
)

func (cmd Command) CheckCommand() (CmdID, error) {
	switch cmd {
	case CmdStatus:
		return CMD_STATUS, nil
	case CmdReload:
		return CMD_RELOAD, nil
	case CmdRestart:
		return CMD_RESTART, nil
	case CmdStart:
		return CMD_START, nil
	case CmdStop:
		return CMD_STOP, nil
	default:
		return CMD_UNKNOWN, fmt.Errorf("unknow command")
	}
}

type CtlConn struct {
	Listener net.Listener
	Conn     net.Conn
	IsConn   bool
}

type CtlCommand struct {
	Id        CmdID
	Args      []string
	Program   *Program
	Response  []byte
	FromShell bool
}
