package models

import (
	"os"
	"syscall"
)

const (
	Running    = "running"
	Waiting    = "waiting"
	Reloading  = "reloading"
	Stopping   = "stopping"
	Processing = "processing"
	Restarting = "restarting"
	Error      = "error"

	Terminated = "terminated"
)

type ProcessNode struct {
	Pid        int
	Name       string
	Status     string
	Command    string
	WaitStatus syscall.WaitStatus
	Ps         *os.ProcessState
}
