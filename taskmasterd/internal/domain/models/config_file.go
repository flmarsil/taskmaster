package models

type ConfigFile struct {
	Path          string
	Taskmasterd   Taskmasterd   `yaml:"taskmasterd"`
	Taskmasterctl Taskmasterctl `yaml:"taskmasterctl"`
	Programs      []Program     `yaml:"programs"`
}

type Taskmasterd struct {
	LogFile         string   `yaml:"logfile"`
	LogFileMaxBytes string   `yaml:"logfile_maxbytes"`
	LogFileBackup   uint8    `yaml:"logfile_backup"`
	Umask           uint8    `yaml:"umask"`
	Directory       string   `yaml:"directory"`
	Environnement   []string `yaml:"environment"`
}

type Taskmasterctl struct {
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	Prompt   string `yaml:"prompt"`
}

type Program struct {
	ProcessName           string   `yaml:"process_name"`
	Command               string   `yaml:"command"`
	NumProcs              uint8    `yaml:"numprocs"`
	AutoStart             bool     `yaml:"autostart"`
	AutoRestart           string   `yaml:"autorestart"`
	ExitCodes             uint8    `yaml:"exitcodes"`
	StartSecs             uint8    `yaml:"startsecs"`
	StartRetries          uint8    `yaml:"startretries"`
	StopSignal            string   `yaml:"stopsignal"`
	StopWaitSecs          uint8    `yaml:"stopwaitsecs"`
	StdOutLogFile         string   `yaml:"stdout_logfile"`
	StdOutLogFileMaxBytes string   `yaml:"stdout_logfile_maxbytes"`
	StdOutLogFileBackups  uint8    `yaml:"stdout_logfile_backups"`
	StdOutCaptureMaxBytes string   `yaml:"stdout_capture_maxbytes"`
	StdErrLogFile         string   `yaml:"stderr_logfile"`
	StdErrLogFileMaxBytes string   `yaml:"stderr_logfile_maxbytes"`
	StdErrLogFileBackups  uint8    `yaml:"stderr_logfile_backups"`
	StdErrCaptureMaxBytes string   `yaml:"stderr_capture_maxbytes"`
	Environnement         []string `yaml:"environment"`
	Directory             string   `yaml:"directory"`
	Umask                 uint8    `yaml:"umask"`
}
