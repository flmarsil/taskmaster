
Ce projet doit être réalisé sur une machine virtuelle.
Votre programme doit 


- Le nombre de processus à lancer et à maintenir en fonctionnement
- Quels codes de retour représentent un état de sortie "attendu" ?
- La durée d'exécution du programme après son lancement pour qu'il soit considéré comme étant
"lancé avec succès".
- Quel signal doit être utilisé pour arrêter le programme (c'est-à-dire pour le faire sortir en douceur) ?

# TODO


- shell : édition de ligne, historique, la complétion serait également appréciable.

- savoir à tout moment si ces processus sont vivants ou morts (cela doit être précis).

- avoir un système de logging qui enregistre les événements dans un fichier local (quand un programme est démarré, arrêté, redémarré, quand il meurt inopinément, quand la configuration est rechargée, etc ...).

- Si le programme doit être redémarré toujours, jamais, ou uniquement en cas de sortie inattendue.

✅ Options permettant de supprimer les fichiers stdout/stderr du programme ou de les rediriger vers des fichiers.
✅ Un répertoire de travail à définir avant de lancer le programme
✅ Un umask à définir avant de lancer le programme

- Bug de deconnnexion ctl
