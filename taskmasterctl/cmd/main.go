package main

import (
	"bufio"
	"os"
	"os/signal"
	"taskmaster/taskmasterctl/internal/domain/services/taskmasterctl"
)

const (
	Addr    = "localhost:44442"
	LogFile = ".taskmasterctl_history"
)

func register_signals() {
	sigchnl := make(chan os.Signal, 1)
	signal.Notify(sigchnl)

	go func() {
		for {
			s := <-sigchnl
			taskmasterctl.HandleSignal(s)
		}
	}()
}

func init() {
	register_signals()
	taskmasterctl.InitHistory()
}

func main() {
	// TODO: getting flag input for connection server info

	// get input user from command line
	input := bufio.NewReader(os.Stdin)

	// init taskmasterctl service
	t := taskmasterctl.NewTaskmasterctlService()

	// init connection to the daemon server
	t.Init(Addr)
	defer t.GetConn().Close()

	// infinit loop for write command and read responses of server
	// throught tls connection
	t.RunShell(input)
}
