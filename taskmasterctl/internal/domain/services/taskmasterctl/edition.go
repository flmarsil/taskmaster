package taskmasterctl

import "fmt"

var horizontal_cursor = 0

// ResetHorizontalCursor reset the horizontal cursor to the current command
func ResetHorizontalCursor() {
	// point the cursor to the last command
	horizontal_cursor = 0
}

// BackHorizontalCursor back (to the right) the horizontal cursor
func BackHorizontalCursor(command *string) {
	if command == nil {
		// if no command was given
		horizontal_cursor++
	} else {
		// else a command was given
		// increment the horizontal cursor by the number of right characters
		horizontal_cursor += len(*command)
	}
}

// EditionWriteRight writs the right part of the horizontal cursor
func EditionWriteRight(command *string) {
	if command != nil {
		// if the command is not null
		// print each character of the right part of the command
		for ; horizontal_cursor < len(*command); horizontal_cursor++ {
			fmt.Printf("%c", (*command)[horizontal_cursor])
		}
	}
}

// FrontHorizontalCursor front (to the left) the horizontal cursor
func FrontHorizontalCursor() bool {
	if horizontal_cursor > 0 {
		// if the horizontal cursor is greater than 0
		// decrement it
		horizontal_cursor--

		return true
	}

	return false
}

// EditionBackSpace modify the command by remove the character before the horizontal cursor
func EditionBackSpace(command *string) (right string, ok bool) {
	if horizontal_cursor > 0 {
		// if the horizontal cursor is greater than 0
		initial_command := *command
		// get the left part of the command relative to the cursor minus one
		left := initial_command[:horizontal_cursor-1]
		// get the right part of the command relative to the cursor
		right = initial_command[horizontal_cursor:]
		// concatenate the left and right part of the command
		*command = left + right
		ok = true
	}

	return
}

// EditionDelete modify the command by delete the character after the horizontal cursor
func EditionDelete(command *string) (right string, ok bool) {
	if horizontal_cursor < len(*command) {
		initial_command := *command
		// get the left part of the command relative to the cursor
		left := initial_command[:horizontal_cursor]
		// get the right part of the command relative to the cursor plus one
		right = initial_command[horizontal_cursor+1:]
		// concatenate the left and right part of the command
		*command = left + right
		ok = true
	}

	return
}

// EditionAddChar add a char to a command
func EditionAddChar(command *string, char byte) (right string) {
	if horizontal_cursor < len(*command) {
		// if the horizontal cursor is in the command
		initial_command := *command
		// get the left part of the command relative to the cursor
		left := initial_command[:horizontal_cursor]
		// get the right part of the command relative to the cursor
		right = initial_command[horizontal_cursor:]
		// concatenate the left and right part of the command and adding the new character
		*command = left + string(char) + right

		return
	} else {
		// else the horizontal cursor at the end of the command
		*command += string(char)
	}

	return
}

// EditionGetNextChar gets the next char after the current horizontal cursor position in the given command
func EditionGetNextChar(command *string) (next_char byte) {
	if horizontal_cursor < len(*command) {
		// if the horizontal cursor is in the command
		// get the next character relative to the cursor
		next_char = (*command)[horizontal_cursor]
	}

	return
}
