package taskmasterctl

import (
	"fmt"
	"os"
	"os/exec"
	"syscall"
)

// ResetSttyState resets stty state by saning it
func ResetSttyState() {
	// https://gist.github.com/mrnugget/9582788
	cmd := exec.Command("stty", "sane")
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		fmt.Println("failed to sane stty")
	}
}

// HandleSignal handles SIGTEMR nad SINGINT signals
func HandleSignal(signal os.Signal) {
	if signal == syscall.SIGTERM || signal == syscall.SIGINT {
		// if receive a kill or ctrl+c signal
		fmt.Println()
		CleanAndExit(0)
	}
}

// CleanAndExit cleans and stops the program
func CleanAndExit(ret int) {
	ResetSttyState()
	os.Exit(ret)
}
