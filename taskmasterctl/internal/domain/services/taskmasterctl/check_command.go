package taskmasterctl

import (
	"fmt"
	"strings"
)

var exit_command = "exit"
var default_commands = []string{"status", "start", "stop", "restart", "reload"}
var all_commands = append(default_commands, exit_command)

const command_max_length = 7

// CheckCommand checks if a command is known and correct.
// If the "exit" command is recognized, the program exits.
func (t *taskmasterctl) CheckCommand(cmd string) error {
	if cmd == exit_command {
		// if the command is "exit"
		// clean and exit the program
		t.conn.Close()
		CleanAndExit(0)
	} else {
		for _, default_command := range default_commands {
			// for each known command
			if cmd == default_command {
				// if a command is finded
				return nil
			}
		}
	}

	// no known command was finded
	return fmt.Errorf("unknow command : '%v'", cmd)
}

// checkCompleteOverlay check if an overlay exist for an command completion
func (t *taskmasterctl) checkCompleteOverlay(char byte, position int) (length int, overlay bool) {
	for i := 0; i < command_max_length; i++ {
		// for each index of advancement in the overlay searching

		match := 0
		local_position := position + i

		for _, command := range all_commands {
			// for each known commands

			if local_position < len(command) && command[local_position] == char {
				// if a character match
				// increment matchs for the current index
				match++
			}
			if match > 1 {
				// if there is more than one match finded
				// an overlay is finded
				return i, true
			}
		}
	}

	return
}

// CompleteCommand try to complet without potential overlays a command
func (t *taskmasterctl) CompleteCommand(cmd string) (missing string, ok bool, overlay bool) {
	// remove potential space before the command
	cmd = strings.TrimLeft(cmd, " ")

	for i := len(cmd); i > 0; i-- {
		// for each index of advancement in the completion searching
		finded_command := ""
		match := 0

		for _, command := range all_commands {
			// for each known commands
			if i <= len(command) && cmd == command[:i] {
				// if a character match
				// increment matchs for the current index
				match++
				// saved the matched command
				finded_command = command

			}
		}

		if match > 1 {
			// if there is more than one match finded
			if length, overlay := t.checkCompleteOverlay(cmd[i-1], i-1); overlay {
				// if an overlay is finded
				// cut the completion before the overlay
				return finded_command[i : i+length], true, true
			}
		}
		if match > 0 {
			// if a match is finded
			return finded_command[i:], true, false
		}
	}

	return
}
