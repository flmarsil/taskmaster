package taskmasterctl

import (
	"crypto/tls"
)

func (t *taskmasterctl) GetConn() *tls.Conn {
	return t.conn
}
