package taskmasterctl

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"os/exec"
	"strings"
)

const buff_size = 5

const (
	KEY_basic = iota
	KEY_tabulation
	KEY_enter
	KEY_delete
	KEY_backspace
	KEY_arrow_up
	KEY_arrow_down
	KEY_arrow_right
	KEY_arrow_left
)

// isSpecialByte determines is a character is special
func isSpecialByte(buff []byte) byte {
	switch buff[0] {
	case 9:
		// TABULATION
		return KEY_tabulation
	case 10:
		// ENTER
		return KEY_enter
	case 127:
		// BACKSPACE
		return KEY_backspace
	case 27:
		if buff[1] == 91 {
			switch buff[2] {
			case 51:
				if buff[3] == 126 {
					// DELETE
					return KEY_delete
				}
				break
			case 65:
				// ARROW UP
				return KEY_arrow_up
			case 66:
				// ARROW DOWN
				return KEY_arrow_down
			case 67:
				// ARROW RIGHT
				return KEY_arrow_right
			case 68:
				// ARROW LEFT
				return KEY_arrow_left
			}
		}
		break
	}

	return KEY_basic
}

// cleanBuff cleans a buffer with the \0 byte
func cleanBuff(buff []byte, size int) {
	for i := 0; i < size; i++ {
		buff[i] = 0
	}
}

// sendCommand sends a command to the controller
func (t *taskmasterctl) sendCommand(command string) {
	// remove potential spaces at the left and right of the command
	command = strings.Trim(command, " ")

	buff := make([]byte, 4096)

	// split each word
	args := strings.Split(command, " ")

	// get first word of input user which represent name command
	cmd := strings.TrimSpace(args[0])

	// check command validity
	fmt.Println(command)
	if err := t.CheckCommand(cmd); err != nil {
		fmt.Println(err)
	} else {
		// send input user to daemon server
		toto, err := t.conn.Write([]byte(command))
		if err != nil {
			if errors.Is(err, net.ErrClosed) {
				fmt.Println("connection was closed by the server [2]")
				CleanAndExit(0)
			}
			fmt.Printf("write error %d : %v\n", toto, err)
			CleanAndExit(1)
		} else {
			// received response from daemon server
			nBytes, err := t.conn.Read(buff)
			if err != nil {
				if err == io.EOF {
					fmt.Println("connection was closed by the server [3]")
					CleanAndExit(0)
				}
				fmt.Printf("read error : %v\n", err)
				CleanAndExit(1)
			}

			if nBytes == 0 {
				fmt.Println("connection was closed by the server [4]")
				CleanAndExit(0)
			}

			fmt.Println("nBytes:", nBytes)

			// print response in stdout
			fmt.Println(string(buff[0:nBytes]))
		}
	}
}

// cleanPrompt clean the prompte by deleting one by one the current command
func cleanPrompt(current_str string) {
	current_str_length := len(current_str)
	if current_str_length > 0 {
		for i := 0; i < current_str_length; i++ {
			fmt.Print("\b \b")
		}
	}
}

// RunShell start to run the shell functions
func (t *taskmasterctl) RunShell(input *bufio.Reader) {
	var buff []byte = make([]byte, buff_size)
	var saved_command string
	var command string

	// disable input buffering
	exec.Command("stty", "-F", "/dev/tty", "cbreak", "min", "1").Run()
	// do not display entered characters on the screen
	exec.Command("stty", "-F", "/dev/tty", "-echo").Run()

	fmt.Printf("%v > ", t.prompt)
	for {
		cleanBuff(buff, buff_size)

		// get input text from user command line
		if _, err := os.Stdin.Read(buff); err != nil {
			fmt.Println("failed to read stdin byte")
		}

		key := isSpecialByte(buff)

		switch key {
		case KEY_backspace:
			// BACKSPACE
			if right, ok := EditionBackSpace(&command); ok {
				// remove the current left character
				fmt.Print("\b \b")
				// print the new right part of the command
				fmt.Print(right)
				// remove the last old character of the command
				fmt.Print(" \b")
				for i := 0; i < len(right); i++ {
					// back the write cursor to his initial position
					fmt.Print("\b")
				}
				// front the horizontal cursor to the removed character
				FrontHorizontalCursor()
			}
			break
		case KEY_delete:
			// DELETE
			if right, ok := EditionDelete(&command); ok {
				// erase the right part of the command by his new
				fmt.Print(right)
				// remove the last old character of the command
				fmt.Print(" \b")
				for i := 0; i < len(right); i++ {
					// back the write cursor to his initial position
					fmt.Print("\b")
				}
			}
			break
		case KEY_basic:
			// BASIC
			right := EditionAddChar(&command, buff[0])
			if GetVerticalCursor() == 0 {
				// if the vertical cursor is at 0
				// saved the current command
				saved_command = command
			}
			// back the horizontal cursor
			BackHorizontalCursor(nil)
			// print the new basic character
			fmt.Printf("%c", buff[0])
			// print the new basic character
			fmt.Print(right)
			for i := 0; i < len(right); i++ {
				// back the write cursor to his initial position
				fmt.Print("\b")
			}
			break

		case KEY_tabulation:
			// TABULATION
			// check if the current command can be completed
			if missing, ok, overlay := t.CompleteCommand(command); ok {
				// if the current command can be completed
				if overlay {
					// if an overlay exists
					// play the bell sound
					fmt.Printf("%c", 7)
				} else {
					// else add a space for continue the command with args
					missing += " "
				}
				// print the missing part of the command
				fmt.Print(missing)
				// add the missing part of the command
				command += missing
				// save the concatened command
				saved_command = command
				// back the cursor to the end of the concatened command
				BackHorizontalCursor(&missing)
			} else {
				// else play the bell sound
				fmt.Printf("%c", 7)
			}

			break
		case KEY_enter:
			// ENTER
			// write the right part of the command
			EditionWriteRight(&command)
			// clean the prompte
			cleanPrompt(command)
			// push the command in the history
			PushCommand(command)
			// reset the vertical cursor
			ResetVerticalCursor()
			// and the horizontal cursor
			ResetHorizontalCursor()
			// send the command to the controller
			t.sendCommand(command)
			// reset command and saved command value
			command = ""
			saved_command = ""
			// display a new prompt
			fmt.Printf("%v > ", t.prompt)
			break
		case KEY_arrow_up:
			// ARROW UP
			// back the vertical cursor to the previous command if exists
			BackVerticalCursor()
			// write the right part of the command
			EditionWriteRight(&command)
			// clean the current command
			cleanPrompt(command)
			// reset the horizontal cursor
			ResetHorizontalCursor()
			if GetVerticalCursor() == 0 {
				// if the vertical cursor is 0 because the history is empty
				// command don't change and reset using the saved command
				command = saved_command
			} else {
				// else the verical cursor changed on the previous one
				// and the command take the value of this previous command
				command = GetPointedCommand()
			}
			// prints the current command
			fmt.Print(command)
			// put the horizontal cursor at the end
			BackHorizontalCursor(&command)
			break
		case KEY_arrow_down:
			// ARROW DOWN
			FrontVerticalCursor()
			// write the right part of the command
			EditionWriteRight(&command)
			// clean the current command
			cleanPrompt(command)
			// reset the horizontal cursor
			ResetHorizontalCursor()
			if GetVerticalCursor() == 0 {
				// if the vertical cursor is 0 because the history is empty
				// command don't change and reset using the saved command
				command = saved_command
			} else {
				// else the verical cursor changed on the previous one
				// and the command take the value of this previous command
				command = GetPointedCommand()
			}
			// prints the current command
			fmt.Print(command)
			// put the horizontal cursor at the end
			BackHorizontalCursor(&command)
			break
		case KEY_arrow_right:
			// ARROW RIGHT

			// get the next character (if exists) of the horizontal cursor
			next_char := EditionGetNextChar(&command)

			if next_char != 0 {
				// if the next character exists
				// print it
				fmt.Printf("%c", next_char)
				// back the horizontal cursor to it
				BackHorizontalCursor(nil)
			}
			break
		case KEY_arrow_left:
			// ARROW LEFT

			// front the horizontal cursor if possible
			if FrontHorizontalCursor() {
				// if the horizontal cursor was decremented
				// move the cursor the left
				fmt.Print("\b")
			}
			break
		}
	}
}
