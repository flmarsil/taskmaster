package taskmasterctl

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func (t *taskmasterctl) GetInputCredentials() (string, string, error) {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Username: ")
	username, err := reader.ReadString('\n')
	if err != nil {
		return "", "", err
	}

	fmt.Print("Password: ")
	password, err := reader.ReadString('\n')
	// bytePassword, err := term.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return "", "", err
	}

	// password := string(bytePassword)
	return strings.TrimSpace(username), strings.TrimSpace(password), nil
}
