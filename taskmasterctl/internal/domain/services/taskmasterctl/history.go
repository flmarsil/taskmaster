package taskmasterctl

import (
	"fmt"
	"log"
	"os"
)

const (
	LogFile = ".taskmasterctl_history"
)

var commands_history = []string{}
var vertical_cursor = 0

func InitHistory() {
	// open the log file
	f, err := os.OpenFile(LogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}

	// remove timestamp flags
	log.SetFlags(0)
	// redirect to the log file
	log.SetOutput(f)
}

// ResetVerticalCursor reset the vertical cursor to the current command
func ResetVerticalCursor() {
	// point the cursor to the last command
	vertical_cursor = 0
}

// Vertical back the vertical cursor in the history
func BackVerticalCursor() {
	if vertical_cursor < len(commands_history) {
		// if the cursor is not bigger than the history length
		vertical_cursor++
	}
}

// FrontVerticalCursor front the vertical cursor in the history
func FrontVerticalCursor() {
	if vertical_cursor > 0 {
		// if the not null or negative
		vertical_cursor--
	}
}

// GetPointedCommand retrieves the command pointed by the vertical cursor
func GetPointedCommand() string {
	return commands_history[len(commands_history)-vertical_cursor]
}

// GetVerticalCursor return the vertical cursor value
func GetVerticalCursor() int {
	return vertical_cursor
}

// PushCommand push a command in the history
func PushCommand(command string) {
	if len(command) > 0 {
		commands_history = append(commands_history, command)
	}
}

// PrintHistory prints all commands saved in the history
func PrintHistory() {
	fmt.Println("----------------- [HISTORY] vertical cursor:", vertical_cursor)
	for _, command := range commands_history {
		fmt.Println(command)
	}
	fmt.Println("-----------------")
}
