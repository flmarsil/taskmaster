package taskmasterctl

import (
	"bufio"
	"crypto/tls"
)

type Taskmasterctl interface {
	Init(addr string)
	RunShell(input *bufio.Reader)
	CheckCommand(cmd string) error
	GetConn() *tls.Conn
	GetInputCredentials() (string, string, error)
}

type taskmasterctl struct {
	conn   *tls.Conn
	prompt string
}

func NewTaskmasterctlService() Taskmasterctl {
	return &taskmasterctl{prompt: "taskmasterctl", conn: nil}
}
