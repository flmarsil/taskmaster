package taskmasterctl

import (
	"crypto/tls"
	"fmt"
	"io"
	"log"
)

func (t *taskmasterctl) Init(addr string) {
	var err error

	config := tls.Config{InsecureSkipVerify: true}

	t.conn, err = tls.Dial("tcp", addr, &config)
	if err != nil {
		fmt.Println("connection to the sever failed [1]")
		CleanAndExit(1)
	}

	// receive prompt from server
	buf := make([]byte, 512)

	nBytes, err := t.conn.Read(buf)
	if err != nil {
		if err != io.EOF {
			fmt.Println("connection was closed by the server [1]")
		} else {
			fmt.Println("connection to the sever failed [2]")
		}
		CleanAndExit(1)
	}

	if nBytes != 0 {
		t.prompt = string(buf[0:nBytes])
	}

	// TODO: ne pas entrer dans cette boucle si pas de creds dans le fichier
	// de configuration dans la partie taskmasterctl

	// Connection loop to the daemon server shell
	isLog := false
	for !isLog {
		// Request the user's login information
		username, password, err := t.GetInputCredentials()
		if err != nil {
			log.Fatalf(err.Error())
		}

		// Send credentials to daemon server
		t.conn.Write([]byte(username + ":" + password))

		// Retrieve the response from the daemon server
		nBytes, err = t.conn.Read(buf)
		if err != nil {
			if err != io.EOF {
				fmt.Printf("read error : %v\n", err)
			}
		}

		response := string(buf[0:nBytes])
		// TODO: changer le ok par une random string envoye par le server
		// au premier read de la fonction avec le prompt
		if response == "ok" {
			isLog = true
		}
	}
}
